FROM debian:bullseye AS build
RUN apt-get -q update \
  && env DEBIAN_FRONTEND=noninteractive apt-get install -qy --no-install-recommends \
  build-essential \
  pkg-config \
  golang-go \
  git \
  ca-certificates \
  openvpn \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

ADD . /src
RUN cd /src && go build -o vpnweb && strip vpnweb

FROM registry.git.autistici.org/ai3/docker/s6-base

COPY --from=build /src/vpnweb /usr/local/bin/vpnweb
COPY docker/conf /etc/
